#!/bin/sh
sed -i \
         -e 's/rgb(0%,0%,0%)/#e8e8e7/g' \
         -e 's/rgb(100%,100%,100%)/#2e3436/g' \
    -e 's/rgb(50%,0%,0%)/#e8e8e7/g' \
     -e 's/rgb(0%,50%,0%)/#4a90d9/g' \
 -e 's/rgb(0%,50.196078%,0%)/#4a90d9/g' \
     -e 's/rgb(50%,0%,50%)/#ffffff/g' \
 -e 's/rgb(50.196078%,0%,50.196078%)/#ffffff/g' \
     -e 's/rgb(0%,0%,50%)/#000000/g' \
	"$@"
